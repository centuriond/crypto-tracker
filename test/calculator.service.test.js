const assert = require('assert')

const service = require('../src/services/calculator.service')


describe('calculator service tests', () => {

    describe('cap gains tests', () => {

        const defaultCapGains = [
            {
                date: new Date('2018-01-01'),
                amount: 0.25,
                price: 99,
                fee: 1
            },
            {
                date: new Date('2018-05-20'),
                amount: 0.50,
                price: 495,
                fee: 5
            },
            {
                date: new Date('2018-06-01'),
                amount: 0.5,
                price: 200,
                fee: 0
            },
            {
                date: new Date('2018-06-25'),
                amount: 0.25,
                price: 400,
                fee: 10
            }
        ]

        it('should calculate FIFO gains correctly', () => {
            const result = service.capitalGains([...defaultCapGains])
            assert.equal(result, -10)
        })


    })


})


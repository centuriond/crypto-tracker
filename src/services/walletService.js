const config = require('../../config')

const CURRENCY_MAPPINGS = Object.freeze({
  ada: require('../providers/blockchains/cardano'),
  atom: require('../providers/blockchains/cosmos'),
  btc: require('../providers/blockchains/bitcoin'),
  eth: require('../providers/blockchains/ethereum'),
  ltc: require('../providers/blockchains/litecoin'),
  vet: require('../providers/blockchains/vechain'),
  xlm: require('../providers/blockchains/stellar')
})


function getClient(currency = '') {
  const client = CURRENCY_MAPPINGS[currency.toLowerCase()]
  if (!client) {
    throw new Error('Unsupported currency')
  }

  return client
}


async function doAction(walletConfigs, processingFn = () => [] ) {
  const walletPromises = walletConfigs.map(walletConfig => {
    const { currency, addresses } = walletConfig
    
    return Promise.all(
      addresses.map(
        addressConfig => processingFn(currency, addressConfig)
      )
    )
  })

  return (await Promise.all(walletPromises)).flat(Infinity)
}


async function getAddressBalance(currency, { address, displayName }) {
  const client = getClient(currency)
  if (!client.getCurrentBalance) throw new Error('Client Provider does not support get Current Balance')

  // VeChain is one address that produces 2 balances (1 for VeChain, 1 for VeThor)
  const unflatWbs = await client.getCurrentBalance(address)
  const walletBalances = Array.isArray(unflatWbs) ? unflatWbs : [unflatWbs]

  return walletBalances.map(wb => ({ ...wb, displayName }) )
}


async function getBalance(currency) {
  const c = config.getWalletConfig(currency)
  const results = await doAction([c], getAddressBalance)

  // any nested arrays to one single level array
  return results
}

async function getAllBalances() {
  const walletConfigs = config.getWalletConfig()
  const results = await doAction(walletConfigs, getAddressBalance)

  return results
}



async function getWalletTransactions(currency, addressConfig) {
  const client = getClient(currency)

  const { address } = addressConfig
  const txs = await client.getTxs(address)

  return {
    address,
    currency,
    txs
  }
}

async function getAllTransactions() {
  const allWalletConfigs = config.getWalletConfig()

  const results = await doAction(allWalletConfigs, getWalletTransactions)

  const txMap = {}
  results.forEach((wTx) => {
    txMap[wTx.currency] = wTx.txs
  })

  return txMap
}

module.exports = {
  getAllBalances,
  getAllTransactions,
  getBalance
}

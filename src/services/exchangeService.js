const binanceClient = require('../providers/exhanges/binance-us')
const coinbaseClient = require('../providers/exhanges/coinbase')
const coinbaseProClient = require('../providers/exhanges/coinbase-pro')


/**
 * 
 * @param {*} currency 
 */
async function getHistoryForCurrency(currency = '') {
  const binanceCurrency = (currency.endsWith('usd') ? currency : currency + 'usd').toUpperCase()

  // todo - don't all fail maybe?
  const [binanceTxs, cbTxs, cbpTxs] = await Promise.all([
    binanceClient.getTransactionHistory(binanceCurrency),
    coinbaseClient.getTransactionHistory(currency),
    coinbaseProClient.getTransactionHistory(currency)
  ])

  const allHistories = [...binanceTxs, ...cbTxs, ...cbpTxs]
  allHistories.sort((a, b) => a.createdAt.getTime() - b.createdAt.getTime())

  return allHistories
}


module.exports = {
  getHistories: getHistoryForCurrency
}


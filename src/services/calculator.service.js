// TODO: cost-basis (FIFO, LIFO)


// FIFO (by year, by pairing)
// price + fee (not network)


function capitalGains(purchases) {
  // FIFO
  // find amount that fits each sell starting at beginning
  
  


  purchases.find(p => p.amount < 0)



  return -15
}






function calculate(config) {
  const { transactions = [], to = 'btc',  from = 'usd' } = config

  // Does not handle:
  //    conversions from coinbase's quizzes
  //    non-fiat trades (ethbtc, compbtc, etc)

  function isSupportedTransaction(t) { 
    const isSupportedTrade = (t.type === 'trade' && (t.trade.pairing === `${to}${from}` || t.trade.pairing === to))
    if (isSupportedTrade && t.trade.from === 'conversion') {
      // do something here eventually
    }    
    
    const isBuyOrSell = t.type === 'buy' || t.type === 'sell'

    return isSupportedTrade || isBuyOrSell
  }

  // filter out the usable transactions

  // really just for debugging reasons to eventually improve our accuracy
  const unsupportedTransactions = transactions
    .filter((t) => !isSupportedTransaction(t))
    .filter((t) => t.type !== 'send')

  let qty = 0
  const purchaseAmounts = []

  const supportedTXs = transactions.filter(isSupportedTransaction)
  supportedTXs.forEach((tx) => {
    const { type } = tx
    const obj = tx[type]

    if (type === 'buy' || type === 'sell') {
      const amount = (type === 'sell' ? -1 : 1) * tx.amount.amount
      qty += amount

      purchaseAmounts.push({
        amount,
        price: obj.unitPrice.amount,
        createdAt: tx.createdAt || null
      })
    }
    
    if (type === 'trade') {
      // TODO - is it always usdTotal?
      // This is fine for now because not many trades are non-usd but 
      // some conversion might be needed for crypto or other non-usd pairings
      const avgPricePer = obj.avgPricePer || (obj.usdTotal / obj.qty)
      const amount = tx.trade.direction === 'buy' ? tx.trade.qty : -1 * tx.trade.qty
      qty += amount

      purchaseAmounts.push({
        amount,
        price: avgPricePer,
        createdAt: tx.createdAt || null
      })
    }
  })


  // calculate weighted avg
  const avgPrice = (purchaseAmounts.reduce((p, c) => p + (c.amount * c.price), 0)) / qty

  // cost basis

  const calculation = {
    avgPrice,
    qty,
    purchaseAmounts,
  }

  return calculation
}



module.exports = {
  calculate,
  capitalGains
}


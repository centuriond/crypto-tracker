const axios = require('axios')

const API_URL = 'https://api.blockcypher.com/v1'

const SAT_TO_LTC = 100_000_000

async function getCurrentBalance(addr) {
  try {
    const { data } = await axios({ url: `${API_URL}/ltc/main/addrs/${addr}/balance`})
    const balance = (data.final_balance || data.balance || 0) / SAT_TO_LTC

    return {
      address: addr,
      balance,
      currency: 'ltc',
      lastQueried: new Date()
    }
  } catch (e) {
    console.log(e)
  }
}

function getAddressList(arr) {
  return Array.from(new Set(arr.reduce((p, c) => p.concat(c.addresses), [])))
}


// no need yet
async function getTxs(addr) {
  const { data } = await axios({ url: `${API_URL}/ltc/main/addrs/${addr}/full?txlimit=500`})

  return data.txs.map((t) => {
    // const isReceiving = t.outputs.filter(o => o.addresses.filter(a => a === addr).length > 0).length > 0
    const isSending = t.inputs.filter(i => i.addresses.filter(a => a === addr).length > 0).length > 0

    const inputAddrs = !isSending ? getAddressList(t.inputs) : [addr]
    const outputAddrs = !isSending ? [addr] : getAddressList(t.outputs)

    let txValue = t.total
    if (!isSending) {
      const rxValues = t.outputs.filter((c) => {
        return c.addresses.filter((a => a === addr)).length > 0
      })

      txValue = rxValues.reduce((p, c) => p + c.value, 0)
    }


    return {
      network: 'litecoin',
      value: {
        unit: 'ltc',
        amount: txValue / SAT_TO_LTC // no no no inputs FROM here
      },
      fee: {
        amount: t.fees / SAT_TO_LTC, // ever more than one?
        unit: 'ltc'
      },
      from: inputAddrs,
      to: outputAddrs, // can't track em all since they're random af
      hash: t.hash,
      memo: '',
      timestamp: new Date(t.confirmed || t.received),
      type: isSending ? 'sent' : 'received'
    }
  })
}


module.exports = {
  getCurrentBalance,
  getTxs
}

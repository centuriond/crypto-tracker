const axios = require('axios')

async function getCurrentBalance(address) {
  const { data } = await axios({
    method: 'post',
    url: `https://graphql-api.mainnet.dandelion.link/`,
    data: {
      query: `query paymentAddressSummay(
        $addresses: [String!]!
        $atBlock: Int
      ) {
        paymentAddresses (addresses: $addresses) {
          summary (atBlock: $atBlock){
            assetBalances {
              assetName
              quantity
            }
          }
        }
      }`,
      variables: {
        addresses: [address]
      }
    }
  })

  // no idea if this'll mean multiple currencies are sent to one address 
  // or if this is just because multiple addresses can be sent back.
  // log if there are ever more than 1
  if (data.data.paymentAddresses.length > 1) {
    console.log('Cardano multiple paymentAddresses')
  }
  
  const wallets = data.data.paymentAddresses.reduce((p, c) => {
    const summary = c.summary
    const assets = summary.assetBalances.map((ab => {
      return {
        currency: ab.assetName, // going to potentially be a native currency ON cardano
        balance: Number(ab.quantity) / 1_000_000
      }
    }))

    return [...p, ...assets]
  }, [])

  const total = wallets
    .filter(w => w.currency === 'ada')
    .reduce((p, c) => p + c.balance, 0)

  return {
    address,
    balance: total,
    currency: 'ada', // good chance it'll be mixed soon with Mary update
    lastQueried: new Date()
  }
}

function sumTxs(arr) {
  return arr.reduce((p, c) => p + Number(c.value), 0)
}

async function getTxs(address) {
  // apparently these are the blocks that I was in


  const txdata = await axios({
    method: 'post',
    url: 'https://graphql-api.mainnet.dandelion.link/',
    data: {
      query:`query getPaymentAddressTransactions($address: String!, $offset: Int!, $limit: Int!) {
          transactions(where: {_or: [{inputs: {address: {_eq: $address}}}, {outputs: {address: {_eq: $address}}}]}, offset: $offset, limit: $limit) {\n    ...TransactionDetails\n  }\n}\n\nfragment TransactionDetails on Transaction {\n  block {\n    epochNo\n    hash\n    number\n    slotNo\n  }\n  deposit\n  fee\n  hash\n  includedAt\n  inputs {\n    address\n    sourceTxHash\n    sourceTxIndex\n    value\n  }\n  metadata {\n    key\n    value\n  }\n  outputs {\n    address\n    index\n    value\n  }\n  withdrawals {\n    address\n    amount\n  }\n}
      `,"variables":{"address":address,"limit":50,"offset":0}
    }
  })

  return txdata.data.data.transactions.map(t => {

    const inputs = t.inputs.filter(i => i.address === address)  // inputs look like staking events or sending away FROM ${address} wallet
    const outputs = t.outputs.filter(o => o.address === address) // outputs are from ADA TO ${address} wallet

    const txn = {
      network: 'cardano',
      deposit: t.deposit, // when staking, the deposit is paid to the pool
      timestamp: new Date(t.includedAt),
      fee: {
        amount: t.fee / 1_000_000,
        unit: 'ada'
      },
      value: {
        amount: sumTxs(outputs) / 1_000_000,
        unit: 'ada'
      },
      memo: '', // is there one?
      hash: t.hash,
    }

    // so figure out what this thing is doing
    if (inputs.length > 0 && outputs.length > 0) {
      const type = 
        t.deposit !== 0 ? 
          t.deposit > 0 ? 'staked' : 'unstaked'
        : 'transferred'
      txn.to = inputs[0].address
      txn.from = outputs[0].address
      txn.type = type // no guarantee of staking - can also just have been in the same block
      // do a search to see if this transaction was a staking op?

    } else {
      if (outputs.length > 0) {
        txn.from = 'not sure'
        txn.to = outputs[0].address
        txn.type = 'received'
      }

      // staking or sending (total of outs = ins )
      if (inputs.length > 0) {
        txn.to = inputs[0].address
        txn.from = address
        txn.type = 'sent' // to be fixed - guessing if outs AND ins are present, I'll assume it's a stake?
      }
    }

    return txn
  })
}

module.exports = {
  getCurrentBalance,
  getTxs
}

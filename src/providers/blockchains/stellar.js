// https://developers.stellar.org/api/introduction/
const axios = require('axios')


const BASE_API_URL = 'https://horizon.stellar.org'


async function getCurrentBalance(addr) {
  const xlmWallte = await axios({
    url: `${BASE_API_URL}/accounts/${addr}`
  })

  console.log(xlmWallte)

  const { data } = xlmWallte

  const balance = (data.balances || [])
    .filter((b) => b.asset_type === 'native')
    .reduce((p, c) => p + +c.balance, 0)

  return {
    address: data.id,
    balance,
    currency: 'xlm',
    lastQueried: new Date()
  }
}

async function getTxs() {
  return {}
}

module.exports = {
  getCurrentBalance,
  getTxs
}

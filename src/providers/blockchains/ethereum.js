const { utils } = require('web3')

// todo remove this
const etherScanApi = require('etherscan-api').init('2RFNJI5AKCKTPBKA8QXEZUHPR6S8FBVTVI')

async function getCurrentBalance(addr) {
  const data = await etherScanApi.account.balance(addr)
  const eth = Number(utils.fromWei(data.result || 0))

  // no multisig that I know of

  return {
    address: addr,
    balance: eth,
    currency: 'eth',
    lastQueried: new Date()
  }
}


async function getTxs(addr) {
  const data = await etherScanApi.account.txlist(addr, 0, Number.MAX_SAFE_INTEGER, 1, 0, 'desc')
  const txs = data.result.map((t) => {
    const type = t.from === addr ? 'sent' : 'received'

    return {
      network: 'ethereum',
      value: {
        balance: Number(utils.fromWei(t.value)),
        unit: 'eth'
      },
      fee: {
        amount: Number(t.gasUsed),
        unit: 'eth'
      },
      from: t.from,
      to: t.to,
      hash: t.hash,
      memo: '',
      timestamp: new Date(Number(t.timeStamp) * 1000),
      type
    }
  })

  return txs

}

module.exports = {
  getCurrentBalance,
  getTxs
}

const axios = require('axios')
const { addressesFromExtPubKey } = require('@swan-bitcoin/xpub-lib')

const API_URL = 'https://api.blockcypher.com/v1'

const SAT_TO_BTC = 100_000_000

const Purpose = Object.freeze({
  P2PKH: '44', // BIP 44: Pay To Pubkey Hash (addresses starting with 1)
  P2SH: '49', // BIP 49: Pay To Witness Pubkey Hash nested in Pay To Script Hash (addresses starting with 3)
  P2WPKH: '84' // BIP 84: Pay To Witness Pubkey Hash (addresses starting with bc1)
})

// converts xpub into consumable addresses
function deriveMultisigWallet (multisigAddress) {
  const attempt = {
    extPubKey: multisigAddress,
    network: 'mainnet',
    addressCount: 3,
    purpose: Purpose.P2PKH
  }
  
  // important! ypub are P2PKH compatible so if no transactions or balances are found, try the fallback
  if (multisigAddress.startsWith('ypub')) attempt.purpose = Purpose.P2SH
  if (multisigAddress.startsWith('zpub')) attempt.purpose = Purpose.P2WPKH

  return addressesFromExtPubKey(attempt).map(a => a.address)
}

async function getCurrentBalance(addr) {
  const isMultiSig = addr.startsWith('xpub') || addr.startsWith('ypub') || addr.startsWith('zpub')
  const addresses = isMultiSig ? deriveMultisigWallet(addr) : [addr]

  const walletResults = await Promise.all(
    addresses.map(async a => {
      try {
        const { data } = await axios({ url: `${API_URL}/btc/main/addrs/${a}/balance?token=06a8a862b4df4f8fb9a2a1065892caf5`})
        const balance = data.final_balance || data.balance || 0
        
        return {
          address: a,
          balance
        }
      } catch (e) {
        console.log(e) // do what?
      }
    })
  )

  const meta = {}

  if (isMultiSig) {
    meta.multisig = addr
  }

  return walletResults
    .map(w => {
      return {
        address: w.address,
        balance: w.balance / SAT_TO_BTC,
        currency: 'btc',
        lastQueried: new Date(),
        meta: {
          multisig: addr
        }
      }
    })
}


function getAddressList(arr) {
  return Array.from(new Set(arr.reduce((p, c) => p.concat(c.addresses), [])))
}

// does not support multisig yet
async function getTxs(addr) {
  const isMultiSig = addr.startsWith('xpub') || addr.startsWith('ypub') || addr.startsWith('zpub')
  const addresses = isMultiSig ? deriveMultisigWallet(addr) : [addr]

  const walletResults = await Promise.all(
    addresses.map(async a => {
      const { data } = await axios({ url: `${API_URL}/btc/main/addrs/${a}/full?txlimit=500`})

      return data.txs.map((t) => {
        const isSending = t.inputs.filter(i => i.addresses.filter(a => a === addr).length > 0).length > 0

        const inputAddrs = !isSending ? getAddressList(t.inputs) : [a]
        const outputAddrs = !isSending ? [a] : getAddressList(t.outputs)

        let txValue = t.total
        if (!isSending) {
          const rxValues = t.outputs.filter((c) => {
            return c.addresses.filter((ad => ad === a)).length > 0
          })

          txValue = rxValues.reduce((p, c) => p + c.value, 0)
        }

        return {
          network: 'bitcoin',
          value: {
            unit: 'btc',
            amount: txValue / SAT_TO_BTC // no no no inputs FROM here
          },
          fee: {
            amount: t.fees / SAT_TO_BTC, // ever more than one?
            unit: 'btc'
          },
          from: inputAddrs,
          to: outputAddrs, // can't track em all since they're random af
          hash: t.hash,
          memo: '',
          timestamp: new Date(t.confirmed || t.received),
          type: isSending ? 'sent' : 'received'
        }
      })
    })
  )

  return walletResults.flat()
}


module.exports = {
  getCurrentBalance,
  getTxs
}

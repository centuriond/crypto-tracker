// simple wrapper for cosmos rpc calls
// stargate.cosmos.network

const axios = require('axios').default

// FOR STARGATE
// new rest api is http://34.71.170.158:1317/
const API_URL = 'https://api.cosmos.network'
// const API_URL = 'https://stargate.cosmos.network'

const UATOM_TO_ATOM = 1_000_000

// util, add paging too 
async function makeRequest(url) {
  const { data } = await axios({ url })

  return data
}



// should save the 
async function getDelegatedSum(addr) {
  const data = await makeRequest(`${API_URL}/staking/delegators/${addr}/delegations`)

  /* 
  {
  "height": "5173738",
  "result": [
    {
      "delegator_address": "",
      "validator_address": "cosmosvaloper196ax4vc0lwpxndu9dyhvca7jhxp70rmcvrj90c",
      "shares": "",
      "balance": ""
    },
    {
      "delegator_address": "",
      "validator_address": "cosmosvaloper1n229vhepft6wnkt5tjpwmxdmcnfz55jv3vp77d",
      "shares": "",
      "balance": """
    }
  ]
}
  */

  return data.result.reduce((p, c) => p + Number(c.balance.amount), 0)
}


async function getUnbondingSum(addr) {
  const data = await makeRequest(`${API_URL}/staking/delegators/${addr}/unbonding_delegations`)

  /*
  {
  "height": "",
  "result": [
    {
      "delegator_address": "",
      "validator_address": "",
      "entries": [
        {
          "creation_height": "5122626",
          "completion_time": "2021-03-04T23:12:45.737535442Z",
          "initial_balance": "",
          "balance": ""
        }
      ]
    }
  ]
}
  */
  return data.result
    .map((r) => r.entries.reduce((p, c) => p + Number(c.balance), 0))
    .reduce((p, c) => p + c, 0)
}


async function getRewardsSum(address) {
  const data = await makeRequest(`${API_URL}/distribution/delegators/${address}/rewards`)
  return (data.result.total || []).reduce((p, c) => p + +c.amount, 0) || 0
}

// multiple msgs, fees, or values?
function convertTxs(cosmosTxObj, cosmosAddr) {
  try {
    // types should be standardized (like send, recieve, vote, stake, unstake, etc)
    const { value, type } = cosmosTxObj.tx.value.msg[0]
    const safeType = type.replace('cosmos-sdk/Msg', '').toLowerCase()
    
    const v = safeType === 'vote' ? value : {
      amount: Number(value.amount.amount || value.amount[0].amount),
      unit: value.amount.denom || value.amount[0].denom
    }

    // fee structure probably desired in eventuality
    return {
      network: 'cosmos',
      value: v,
      fee: {
        amount: Number(cosmosTxObj.tx.value.fee.amount[0].amount), // ever more than one?
        unit: cosmosTxObj.tx.value.fee.amount[0].denom
      },
      from: value.from_address || cosmosAddr,
      to: value.delegator_address || value.to_address || value.voter,
      hash: cosmosTxObj.txhash,
      memo: cosmosTxObj.tx.value.memo || '',
      timestamp: new Date(cosmosTxObj.timestamp),
      type: safeType
    }
  } catch (e) {
    console.log(e)
  }
}


// unstaked only
async function getAvailableFunds(cosmosId) {
  const data = await makeRequest(`${API_URL}/auth/accounts/${cosmosId}`)
  const { coins = [] } = data.result.value

  return coins.reduce((p, c) => p + +c.amount, 0)
}

// obviously this isn't ideal BUT it makes it convenient
// probably should cache info locally with ~1min ttl
async function getCurrentBalance(cosmosAddr) {
  const [delegated, unbonding, rewards, available] = await Promise.all([
    getDelegatedSum(cosmosAddr),
    getUnbondingSum(cosmosAddr),
    getRewardsSum(cosmosAddr),
    getAvailableFunds(cosmosAddr)
  ])

  const sum = (delegated + unbonding + rewards + available) / UATOM_TO_ATOM
  const meta = {
    rewards: rewards / UATOM_TO_ATOM,   // available to take as a reward
    delegated: delegated / UATOM_TO_ATOM, // staked
    available: available / UATOM_TO_ATOM, // availble to send around
    unbonding: unbonding / UATOM_TO_ATOM // in the 21 day unstaking
  }

  return {
    address: cosmosAddr,
    balance: sum,
    currency: 'atom',
    lastQueried: new Date(),
    meta
  }

  // return makeRequest(`${API_URL}/bank/balances/${cosmosAddr}`) // is this worthless?
}

// get cosmos transactions
// todo: make into a unified transaction data structure
async function getTxs(cosmosAddr, pagination) {
  const { page = 1, limit = 25 } = pagination || {}
  const data = await makeRequest(`${API_URL}/txs?page=${page}&limit=${limit}&message.sender=${cosmosAddr}`)

  const txs = (data.txs || []).map(e => convertTxs(e, cosmosAddr))

  // not ready to do this yet but might
  const results = {
    currentPage: Number(data.page_number),
    pageTotal: Number(data.page_total),
    total: Number(data.total_count),
    txs
  }

  return txs
}


module.exports = {
  getTxs,
  getAvailableFunds,
  getCurrentBalance
}
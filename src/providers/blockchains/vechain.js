const axios = require('axios')
const queue = require('async/queue')
const { utils } = require('web3')

const BASE_URL = 'https://sync-mainnet.vechain.org'
const VECHAIN_REQUEST_ASYNC = 3

async function getCurrentBalance(addr) {
  const resp = await axios({
    url: `${BASE_URL}/accounts/${addr}`
  })

  const { data } = resp

  // might be too big for number eventually but WEI to Ether drops like 18decimal places
  const balance = Number(utils.fromWei(utils.hexToNumberString(data.balance)))
  const energy = Number(utils.fromWei(utils.hexToNumberString(data.energy)))

  return [
    {
      address: addr, 
      balance,
      currency: 'vet',
      lastQueried: new Date()
    },
    {
      address: addr,
      balance: energy,
      currency: 'vethor',
      lastQueried: new Date(),
    }
  ]
}



// only really used for fees
async function getTxn(txId) {
  const { data } = await axios({
    url: `${BASE_URL}/transactions/${txId}/receipt`
  })

  // no real conversion needed

  return data
}


async function getTxs(addr, pagination) {
  const { offset = 0, limit = 25 } = pagination || {}

  const resp = await axios({
    method: 'post',
    url: `${BASE_URL}/logs/transfer`,
    data: {
      range: {
        unit: 'block',
        from: 0,
        to: Number.MAX_SAFE_INTEGER
      },
      options: { offset, limit },
      criteriaSet: [
        { sender: addr }, 
        {  recipient: addr }
        // txOrigin: addr, no idea what this one is
      ],
      order: 'desc'
    }
  })
  

  const txns = []

  const requestQueue = queue(async (t, cb) => {
    const txn = await getTxn(t.meta.txID)
  
    txns.push({
      network: 'vechain',
      fee: {
        amount: Number(txn.gasUsed) / 1000 ,
        unit: 'vetho'
      },
      value: {
        amount: Number(utils.fromWei(utils.hexToNumberString(t.amount))),
        unit: 'vet'
      },
      hash: t.meta.txID,
      timestamp: new Date(t.meta.blockTimestamp * 1000), // is that good enough or do I need the transaction's time too from the txID?
      memo: '', // Memos aren't used
      type: 'txn', // no standard here? maybe look at addresses to see what I'm looking FOR
      from: t.sender,
      to: t.recipient
    })

    sleep(() => { cb() }, 300)
  }, VECHAIN_REQUEST_ASYNC)

  // queue em up and wait
  requestQueue.push(resp.data)
  await requestQueue.drain()

  return txns
}



module.exports = {
  getCurrentBalance,
  getTxs
}
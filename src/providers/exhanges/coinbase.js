const axios = require('axios')
const {
  getApiKey
} = require('../../../config')
const utils = require('../../utils/signing')

const {
  apiKey,
  apiSecret
} = getApiKey('coinbase')

const API_URL = 'https://api.coinbase.com'
const API_VERSION = '2021-02-12'

async function makeRequest(reqPath, method = 'get', body = {}) {
  // must be in SECONDS since Unix epoch
  const timestamp = Math.floor(new Date().getTime() / 1000)

  // omit body if get
  const bodyJson = method === 'get' ? '' : JSON.stringify(body)

  // as defined: https://developers.coinbase.com/api/v2?shell#authentication
  const stringToSign = `${timestamp}${method.toUpperCase()}${reqPath}${bodyJson}`
  const cpAccessSign = utils.signData(apiSecret, stringToSign)

  const temp = await axios({
    url: `${API_URL}${reqPath}`,
    method,
    headers: {
      'CB-ACCESS-KEY': apiKey,
      'CB-ACCESS-SIGN': cpAccessSign,
      'CB-ACCESS-TIMESTAMP': timestamp,
      'CB-VERSION': API_VERSION
    }
  })

  return temp.data
}


async function listAccounts() {
  const accounts = await makeRequest('/v2/accounts?limit=100')

  // paging to get all?

  return accounts.data.map(a => {
    const balance = Number(a.balance.amount)
    const currency = a.balance.currency.toLowerCase()

    // there is an allow_withdrawals but don't see an amount withheld
    const hold = 0 // not sure there's an amount provided...maybe need to get each currency with balance > 0 to see

    return {
      available: balance, // until I can see what's available vs not...
      balance,
      currency,
      hold,
      lastQueried: new Date(),
      meta: {
        id: a.id // used to get this asset's Txs I'm guessing
      },
      source: 'coinbase'
    }
  })
}

/**
 * Withdrawals are Coinbase to bank
 */
async function getWithdrawals() {
  const allAccounts = await listAccounts()

  // sad face - forced USD
  const usd = allAccounts.find(acc => acc.currency === 'btc')

  const withdrawals = await makeRequest(`/v2/accounts/${usd.meta.id}/withdrawals`)

  return withdrawals.data.map(w => {
    return {
      id: w.id,
      transactionId: w.transaction.id,
      createdAt: new Date(w.create_at),
      type: 'withdrawal',
      amount: {
        amount: Number(w.amount.amount),
        currency: w.amount.currency.toLowerCase()
      },
      fee: {
        amount: Number(w.fee.amount),
        currency: w.fee.currency.toLowerCase()
      },
      holdUntil: new Date(w.hold_until)
    }
  })
}

/**
 * Deposits are from a bank to Coinbase
 */
async function getDeposits() {
  const allAccounts = await listAccounts()

  // sad face - forced USD
  const usd = allAccounts.find(acc => acc.currency === 'usd')

  const deposits = await makeRequest(`/v2/accounts/${usd.meta.id}/deposits`)

  return deposits.data.map(d => {
    return {
      id: d.id,
      transactionId: d.transaction.id,
      createdAt: new Date(d.create_at),
      type: 'deposit',
      amount: {
        amount: Number(d.amount.amount),
        currency: d.amount.currency.toLowerCase()
      },
      fee: {
        amount: Number(d.fee.amount),
        currency: d.fee.currency.toLowerCase()
      },
      holdUntil: new Date(d.hold_until)
    }
  })
}







async function processTx(txRaw) {
  let { amount, created_at, id, native_amount, status, type } = txRaw

  const currency = amount.currency.toLowerCase()
  amount = Number(amount.amount)
  const isSending = amount < 0

  const nativeAmount = {
    amount: Math.abs(Number(native_amount.amount)),
    currency: native_amount.currency.toLowerCase()
  }

  const result = {
    id,
    amount: {
      amount,
      currency
    },
    meta: {
      nativeAmount, // not available in cbp - but cool nonetheless .. maybe will look it up later
    },
    source: 'coinbase',
    createdAt: new Date(created_at),
    status,
    type
  }

  // cool but no info on cbp
  result[type] = {
    description: txRaw.details.title,
    from: txRaw.details.payment_method_name
  }

  if (type !== 'order' && type !== 'send') {
    result[type].id = txRaw[type].id
  }

  if (type === 'buy') {
    const { data } = await makeRequest(txRaw.buy.resource_path)
    const buy = data

    result.buy = {
      id: buy.id,
      fee: {
        amount: Number(buy.fee.amount),
        currency: buy.fee.currency.toLowerCase()
      },
      subtotal: {
        amount: Number(buy.subtotal.amount),
        currency: buy.subtotal.currency.toLowerCase()
      },
      unitPrice: {
        amount: Number(buy.unit_price.amount),
        currency: buy.unit_price.currency.toLowerCase()
        // unit: currency
      }
    }
  }

  if (type === 'trade') {
    result.trade = {
      ...result.trade,
      direction: amount > 0 ? 'buy': 'sell',
      paring: `${currency}`, // this is a conversion though
      from: 'conversion',
      to: currency,
      avgPricePer: 0, // because I don't know from here - maybe the id is in the coinbase pro list
      qty: Math.abs(amount),
      feeTotal: 0, // no cb fee for conversion?
      usdTotal: nativeAmount.amount,
      status: txRaw.status,
      rawTrades: [txRaw]
    }
  }

  if (type === 'send') {
    if (!txRaw.network) {
      console.log(JSON.stringify(txRaw))
    }

    const { network } = txRaw
    // const amount = Math.abs(Number((network.transaction_amount || t.amount).amount))
    // const currency = (network.transaction_amount || t.amount).currency.toLowerCase()
    const networkFee = Number((network.transaction_fee || {}).amount || 0) // 0 means not in Tx

    result.send = {
      hash: txRaw.network.hash,
      to: (txRaw.to || {}).address || 'coinbase',
      fee: {
        amount: networkFee,
        currency: (network.transaction_fee || txRaw.from || {}).currency.toLowerCase()
      },
      total: amount + networkFee,
      status: txRaw.network.status
    }


    if (!isSending && !txRaw.from.address) {
      // this transaction needs additional data which is available on the blockchain
      // but I'm not going to fill in that data here. would complicated things
      result.from = {
        address: txRaw.from.resource || 'unknown',
        amount: 0,
        currency: txRaw.from.currency.toLowerCase()
      }
    }
  }

  return result
}

/**
 * Retreive a list of transfers between Coinbase and somewhere else 
 * which could be a wallet, hardware, etc
 * 
 * @param {String} currency 
 */
async function getTransactionHistory(currency = 'btc') {
  const allAccounts = await listAccounts()
  const account = allAccounts.find(acc => acc.currency.toLowerCase() === currency.toLowerCase())
  
  if (!account) return []
  
  const transactions = await makeRequest(`/v2/accounts/${account.meta.id}/transactions?limit=100`)

  return await Promise.all(
    transactions.data
    .filter(t => t.type !== 'pro_withdrawal' && t.type !== 'pro_deposit') // sends and pro_withdrawals are duplicated BUT pro_withdrawals do the sent to/from
    .map(t => processTx(t))
  )
}



module.exports = {
  listAccounts,
  getDeposits,
  getWithdrawals,
  getTransactionHistory
}
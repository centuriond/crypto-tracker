const axios = require('axios')
const crypto = require('crypto');
const { abort } = require('process');

const { getApiKey } = require('../../../config')
const { apiKey, apiSecret } = getApiKey('binance-us')
const utils = require('../../utils//signing')

const BASE_URL = 'https://api.binance.us'


// make a queue of requests to avoid 429s (shoot for 1req / 250ms?)
// also handle 418s

async function makeRequest(url, params = {}, isSigned = false) {
  let qs = ''

  if (isSigned) {
    params.timestamp = Date.now()
  }

  const e = Object.entries(params)
  for (let i = 0; i < e.length; i++) {
    const [k, v] = e[i]
    qs += `${k}=${v}${i === e.length - 1 ? '' : '&'}`
  }

  if (isSigned) {
    const signed = utils.signData(apiSecret, qs)
    qs += `&signature=${signed}`
  }

  const resp = await axios({
    method: 'get',
    url: `${url}?${qs}`,
    headers: {
      'X-MBX-APIKEY': apiKey
    }
  })

  return resp.data
}

// shows balances
async function listAccounts() {
  const url = `${BASE_URL}/api/v3/account`
  const data = await makeRequest(url, {}, true)


  const balances = data.balances.map(a => {
    const { asset, free, locked } = a

    return {
      available: Number(free),
      balance: Number(free) + Number(locked),
      currency: asset.toLowerCase(),
      hold: Number(locked),
      source: 'binance-us' // should be an ID later
    }
  })


  return balances
}

// likely not needed yet
async function getAllOrderList() {
  const url = `${BASE_URL}/api/v3/allOrderList`
  const data = await makeRequest(url, { endTime: new Date(0).getTime() / 1000 }, true)
  return data
}

// save fromIds to prevent the need for adding additional orders
async function getTransactionHistory(symbol = 'BTCUSD') {
  const url = `${BASE_URL}/api/v3/myTrades`
  const trades = await makeRequest(url, { symbol: symbol.toUpperCase() }, true)

  return trades.map(t => {
    const isPurchase = !!t.isBuyer
    const commissionAmt = Number(isPurchase ? t.qty: t.quoteQty)
    const fundingAmt = Number(isPurchase ? t.quoteQty: t.qty)

    const commissionAsset = t.commissionAsset.toLowerCase()
    const fundingAsset = t.symbol.replace(t.commissionAsset, '').toLowerCase()

    return {
      id: t.orderId,
      source: 'binance-us',
      createdAt: new Date(t.time),
      type: 'trade',
      status: 'complete',
      trade: {
        id: t.orderId,
        direction: isPurchase ? 'buy' : 'sell',
        pairing: t.symbol.toLowerCase(),
        from: commissionAsset,
        to: fundingAsset,
        recievedTotal: commissionAmt,
        avgPricePer: Number(t.price),
        qty: Number(t.qty),
        feeTotal: Number(t.commission),
        usdTotal: Number(fundingAmt), // this won't always be funding but ok for now
        status: 'complete',
        rawTrades: [t]
      }
    }
  })
}


async function getAllMyTrades(arr) {
  // for all 
}


module.exports = {
  listAccounts,
  // listDeposits,
  // listWithdrawals,
  getAllOrderList,
  getAllMyTrades,
  getTransactionHistory
}

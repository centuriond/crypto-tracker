const { all, some } = require('async');
const { CoinbasePro } = require('coinbase-pro-node')

const { getApiKey } = require('../../../config')
// API Keys can be generated here:
// https://pro.coinbase.com/profile/api

const apiAuth = getApiKey('coinbase-pro')

const auth = {
  apiKey: apiAuth.apiKey,
  apiSecret: apiAuth.apiSecret,
  passphrase: apiAuth.apiPass,
  useSandbox: false
}

const client = new CoinbasePro(auth);

/**
 * Retrieves all Coinbase Pro currencies held 
 */
async function listAccounts() {
  const allAccounts = await client.rest.account.listAccounts()

  return allAccounts.map(acct => {
    const { available, balance, currency, hold, id } = acct

    return {
      available: Number(available),
      balance: Number(balance),
      currency: currency.toLowerCase(),
      hold: Number(hold),
      lastQueried: new Date(),
      meta: {
        id // used to get this asset's Txs I'm guessing
      },
      source: 'coinbase-pro'
    }
  })
}


/**
 * Retrieves Coinbase Pro
 */
async function findCurrency(currency = 'btc') {
  const allData = await listAccounts()
  return allData.find(currObj => currObj.currency.toLowerCase() === currency.toLowerCase())
}







/**
 * 
 * @param {String} type
 * @param {Object} txData - general transaction data
 * @param {*} typeData - data specific to the type of transaction (trade, order, etc)
 */
function processTx({ type, txData, typeData, currency }) {
  const result = {
    id: typeData.id || txData.details.order_id,
    source: 'coinbase-pro',
    createdAt: new Date(txData.created_at),
    type,
    status: 'invalid'
  }

  // combine a list of trades from one order
  // since multiple trades fulfill what an order is
  if (type === 'trade') {
    const assets = txData.details.product_id.split('-')

    const direction = typeData[0].side
    const isBuy = direction === 'buy'
    const to = (isBuy ? assets[0] : assets[1]).toLowerCase()
    const from = (isBuy ? assets[1] : assets[0]).toLowerCase()

    const aggData = typeData.reduce((p, c) => {
      const temp = { ...p }
      temp.feeTotal += Number(c.fee),
      temp.avgPrice += Number(c.price) / typeData.length
      temp.usdTotal += Number(c.usd_volume)
      temp.size += Number(c.size)
      return temp
    }, {
      feeTotal: 0,
      avgPrice: 0,
      size: 0,
      usdTotal: 0 // maybe this is native currency?
    })


    const trade = {
      id: txData.details.order_id,
      direction: typeData[0].side,
      pairing: txData.details.product_id.toLowerCase().replace('-', ''),
      from,
      to,
      avgPricePer: aggData.avgPrice,
      qty: aggData.size,
      feeTotal: aggData.feeTotal,
      usdTotal: aggData.usdTotal,
      createdAt: new Date(txData.created_at),
      status: typeData[0].settled ? 'complete' : 'incomplete', //what happens if mixture?
      rawTrades: typeData
    }

    result.status = typeData[0].settled ? 'complete' : 'incomplete',
    result.trade = trade
  }

  if (type === 'send') {
    const amount = Number(typeData.details.subtotal)
    const fee = Number(typeData.details.fee)
    const total = Number(typeData.amount)

    result.send = {
      hash: typeData.details.crypto_transaction_hash,
      amount: {
        amount,
        currency
      },
      fee: {
        amount: fee,
        currency
      },
      to: typeData.details.sent_to_address || 'coinbase',
      total,
      status: 'completed', // check completed date?
      createdAt: new Date(txData.created_at)
    }

    result.status = 'complete' // when isn't it

    // special case because it never left coinbase really
    if (!result.send.hash) {
      result.send.hash = `Coinbase ${txData.amount < 0 ? 'from' : 'to'} Coinbase Pro`
    }
  }

  if (type !== 'send' && type !== 'trade') {
    // something else?
    console.log('wha')
  }

  return result
}




/**
 * Helper to get pro account history and filter the currency directly
 * @param {String} currency 
 */
async function getTransactionHistory(currency = 'usd') {
  const thing = await findCurrency(currency)

  if (!thing) return []

  // all things including deposits / trades
  const { data } = await client.rest.account.getAccountHistory(thing.meta.id)

  // reduce 
  const ids = new Set()
  const toCheck = data.filter((o) => {
    const orderId = o.details.order_id
    if (!orderId) return true

    if (orderId && !ids.has(o.details.order_id)) {
      ids.add(o.details.order_id)
      return true
    }

    return false
  })

  const promises = toCheck.map(async tx => {
    // get completed (filled) trades for this currency 
    if (tx.details.product_id) {
      const tradeData = await client.rest.fill.getFillsByOrderId(tx.details.order_id)
      return processTx({ type: 'trade', txData: tx, typeData: tradeData.data, currency })
    } else if (tx.details.transfer_id) {
      const send = await client.rest.transfer.getTransfer(tx.details.transfer_id)
      return processTx({ type: 'send', txData: tx, typeData: send, currency })
    }

    console.log('no idea')
  })

  const results = await Promise.all(promises)

  return results
}



function mapTransfers(dataObj, type = 'desposit', currency = 'usd') {
  return {
    id: dataObj.id,
    amount: {
      amount: Number(dataObj.amount),
      currency
    },
    createdAt: new Date(dataObj.createdAt),
    type,
    fee: {
      amount: 0,
      currency
    },
    source: 'coinbase-pro',
    holdUntil: 'Unknown' // info not provided - but should be on coinbase - oh well
  }
} 



async function getDeposits(currency = 'usd') {
  const currencyHistory = await findCurrency(currency)
  const { data } = await client.rest.transfer.getTransfers('deposit', currencyHistory.meta.id)
  return data.map(d => mapTransfers(d, 'deposit', currency.toLowerCase()))
}

async function getWithdrawals() {
  const currencyHistory = await findCurrency(currency)
  const { data } = await client.rest.transfer.getTransfers('withdraw', currencyHistory.meta.id)
  return data.map(d => mapTransfers(d, 'withdraw', currency.toLowerCase()))
}

module.exports = {
  // getTransfers,
  listAccounts,
  getDeposits,
  getWithdrawals,
  getTransactionHistory
}

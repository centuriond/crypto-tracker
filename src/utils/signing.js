const crypto = require('crypto')

function signData(secret, data) {
  return crypto.createHmac('sha256', secret).update(data).digest('hex')
}

module.exports.signData = signData

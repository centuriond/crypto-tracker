const walletService = require('./services/walletService')
const exchangeService = require('./services/exchangeService')
const calculatorService = require('./services/calculator.service')
const fs = require('fs')
const path = require('path')

// const db = require('./utils/db')

function fun() {
  return Promise.resolve()
}


fun().then(async () => {
    // const [ balance, histories ] = await Promise.all([
    //   walletService.getBalance('vet'),
    //   exchangeService.getHistories('btc')
    // ])


    const currency = 'btc'.toLowerCase()
    const txHistoryFile = path.join(__dirname, `tx-history-${currency}.json`)

    let histories = []

    // skip lookup if files exist
    if (!fs.existsSync(txHistoryFile)) {
      histories = await exchangeService.getHistories(currency)
      fs.writeFileSync(txHistoryFile, JSON.stringify(histories))
    } else {
      histories = JSON.parse(fs.readFileSync(txHistoryFile))
    }

    const info = calculatorService.calculate({
      transactions: histories,
      to: currency
    })
    
    return null
  })
  .then(() => {
    console.log('done')
  })
  .catch((e) => {
    console.log(e)
  })

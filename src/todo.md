todo: 

- make simple readonly providers for exchanges
- make simple readonly blockchain retrievals
  - btc           (done)
  - ada           (done) - staking is hokey but works
  - atom          (done)
  - ltc           (done)
  - grt
  - algo
  - ren
  - eth           (done)
  - vet/vetho     (done)
  - xtz
  - xlm           (done)
  - xmr           (how possible is this given its hidden wallet balances?)


- M2 (Exchanges - coinbase, coinbase pro, binance-us)
  - balances and trades
  - deposits and withdrawals
  - fees

  <!-- useful for getting all prices against a few "versus currencies" -->
  <!-- https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=bitcoin,ethereum,cosmos -->

- M3 (Timeline and profits)
  - gather transactions, deposits, withdrawals and try to figure out a timeline of
    where, when, and how much went where for each
  - do average cost basis (weighted average)
      - ((#coins * price1) + (# * price2) + (#coins * price3)) / (total coins)
  - caclulate total fees
  - caclulate total network transfer costs
  - calculate OVERALL profits

- M4 (Good citizen)
  - queue request rate limits for APIs - set by client config (for now)

- M5 (Caching and background service)
  - cache requests for at least (tunable per API provider based on rate limits)
    - each provider has its own cache and queuing logic
    - tunable TTLs
    - wallet requests should be prioritized over Txs

  - add scheduled background retrievals fed into cache at regular intervals

- M6 (Rest service - monolith ok)
  - design API and prep for DB need
  - begin UI design

const walletService = require('../services/walletService')



module.exports = function (fastify, _, done) {
  fastify.get('/wallets', async function (req) {
    return walletService.getAllWalletBalances()
  })

  done()
}
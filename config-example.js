const config = Object.freeze({

  // meant to track transactions and fees from exchanges
  exchanges: [
    {
      name: 'coinbase',
      apikey: '',
      apiSecret: '',
    },

    {
      name: 'coinbase-pro',
      apiKey: '',
      apiSecret: '',
      apiPass: ''
    },

    {
      name: 'binance-us',
      apiKey: '',
      apiSecret: '',
      // list of trades I did at some point
      // keeping this local to cut down on calls to this exchange
      currencies: [
        'ADAUSD',
        'ADABTC',
        'ALGOUSD',
        'ATOMUSD',
        'BTCUSD',
        'COMPUSD',
        'ENJUSD',
        'LTCUSD',
        'LINKUSD',
        'VETUSD',
        'XLMUSD'
      ]
    }
  ],

  // intended to be the main wallet holding (regardless of custody)
  wallets: [
    // {
    //   currency: 'btc',
    //   addresses: [{
    //     address: '',
    //     displayName: '',
    //     location: ''
    //   }]
    // },
  ]
})

// sorta silly but fine for now
function getApiKey(name) {
  return config.exchanges.find(e => e.name === name)
}

function getExchangeCurrencyWatchList(name) {
  const ex = config.exchanges.find(e => e.name === name) || {}
  return ex.currencies || []
}

function getWalletAddress(currency = '') {
  if (!currency) return config.wallets

  const a = config.wallets.find(c => c.currency === currency) || {}
  return a.address || null
}

module.exports = {

  getApiKey,
  getExchangeCurrencyWatchList,
  getWalletAddress
}